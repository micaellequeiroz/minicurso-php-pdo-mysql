<?php
include "config.php";

function abrirBanco() {
	try {
		$conexao = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		mysqli_set_charset($conexao, CHARSET);
		return $conexao;
	} catch (Exception $e) {
		echo $e->getMessage();
		return null;
	}
}

function fecharBanco($conexao) {
	try {
		mysqli_close($conexao);
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}

?>