<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tipo de esporte favorito</title>
</head>
<body>	
<table>
  <tr>
    <th>Nome</th>
    <th>Idade</th>
    <th>Sexo</th>
    <th>Esporte Favorito</th>
  </tr>
     <?php 
        include "select.php";
        $enq = selectEnquete();
        if(is_array($enq)){
        foreach ($enq as $enquete => $e) {
          $nome = $e->nome;
          $idade = $e->idade;
          $sexo = $e->sexo;
          $esporte =  $e->esporte_favorito;
?>
  <tr>
    <td><?=$nome;?></td>
    <td><?=$idade;?></td>
    <td><?=$sexo;?></td>
    <td><?=$esporte;?></td>
  </tr>
    <?php }}else {
        echo "Enquete vazia!";
    } ?>
</table>
<a href="../index.html">Voltar</a>
</body>
</html>