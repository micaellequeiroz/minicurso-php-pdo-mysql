<?php
class Enquete
{
    private $nome;
    private $idade;
    private $sexo;
    private $esporte;

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getIdade()
    {
        return $this->idade;
    }

    public function setIdade($idade)
    {
        $this->idade = $idade;
    }

    public function getSexo()
    {
        return $this->sexo;
    }

    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    public function getEsporte()
    {
        return $this->esporte;
    }

    public function setEsporte($esporte)
    {
       $this->esporte = $esporte;
    }

    public function inserir(){
        include_once '../actions/conexao.php';

        $con = NEW Conexao();
        $conn = $con->getConexao();

        $stmt = $conn->prepare("INSERT INTO perfil(nome, idade, sexo, esporte_favorito) VALUES(:NOME, :IDADE, :SEXO, :ESPORTE)");
        $stmt->bindParam(":NOME",$this->nome);
        $stmt->bindParam(":IDADE",$this->idade);
        $stmt->bindParam(":SEXO",$this->sexo);
        $stmt->bindParam(":ESPORTE",$this->esporte);


        $stmt->execute();
        if($stmt){
            echo "<script>alert('Enquete enviada!');</script>";
			exit('<script>location.href = "../index.html"</script>');
        }
        else{
            echo "<script>alert('Erro ao Enviar a Enquete');</script>";
			exit('<script>location.href = "../index.html"</script>');
        }

       // $stmt->close();
    }

}
?>