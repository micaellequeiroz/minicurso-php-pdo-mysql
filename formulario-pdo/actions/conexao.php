<?php
class Conexao
{
    private $server;
    private $user;
    private $pass;

    public function __construct()
    {
        $this->server = 'mysql:host=localhost; dbname=esporte; charset=utf8';
        $this->user = 'root';
        $this->pass = '';
    }
 
   
    function getConexao(){
        try {
            $connect = new PDO($this->server, $this->user, $this->pass);
            return $connect;
        } catch (PDOException $ex) {
            echo 'Error:' . $ex->getMessage();
        }

    }
}
?>